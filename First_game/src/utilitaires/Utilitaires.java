package utilitaires;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Package regroupant les fonctions utilitaires du demineur
 * @author SRet
 *
 */
public class Utilitaires {

	/**
	 * methode permettant de remplir le plateau de mines
	 * @param mines : tableau des mines
	 * @param row : nombre de colonnes voulu par l'utilisateur
	 * @param cols : nombre de lignes voulu par l'utilisateur
	 * @param numMines : nombre de mines voulu par l'utilisateur
	 * @return installe le tableau des mines dans le jeu
	 */
	public static boolean [][] RemplirMines (boolean [][] mines, int row, int cols, int numMines) {
		//ajoute les mines au plateau
		int besoinMine = numMines;
		while (besoinMine>0)
		{int x = (int) Math.floor(Math.random()*row);
		int y = (int) Math.floor(Math.random()*cols);
		if (!mines[x][y]) {
			mines[x][y] = true;
			besoinMine--;
		}
		}
		return mines;}

	/**
	 * methode permettant d'indiquer au joueur le nombre de mines aux alentours
	 * @param minesAlentours : tableau à creer
	 * @param row : nombre de lignes;
	 * @param col : nombre de colonnes;
	 * @param mines : tableau des mines
	 * @return rempli le tableau du nombre de mines alentours dans le jeu
	 */
	public static int [][] RemplirNombres (int [][] minesAlentours, int row, int col, boolean [][] mines ) {
		//ajoute les nombres au minesAlentoursleau
		for (int x = 0; x < row; x++) {
			for (int y = 0; y < col; y++) {
				int count = 0;
				if (mines[x][y] == true) minesAlentours[x][y] = -1;
				if (mines[x][y] == false)
				{	// verifie la case au NO
					{if (x-1>=0 && y-1>=0)
					{ if (mines[x-1][y-1] == true) count++;}

					// verifie la case au N
					if (x-1>=0)
					{ if (mines[x-1][y] == true) count++;}

					// verifie la case au NE 
					if (x-1>=0 && y+1<col)
					{if (mines[x-1][y+1] == true) count++;}

					// verifie la case au O
					if (y-1>=0)
					{if (mines[x][y-1] == true)count++;}

					// verifie la case au E
					if (y+1<col)
					{if (mines[x][y+1] == true) count++;}

					// verifie la case au SO
					if (x+1<row && y-1>=0) 
					{if (mines[x+1][y-1] == true)count++;}	

					//verifie la case au S
					if (x+1<row)
					{if (mines[x+1][y] == true) count++;}

					//verifie la case au SE
					if (x+1<row && y+1<col)
					{if (mines[x+1][y+1] == true) count++;}
					}
					minesAlentours[x][y] =count;
				}
			}
		}
		return minesAlentours;
	}

	/**
	 * Methode verifiant la fin du jeu en incrementant le nombre de cases reussi par 1.
	 * Si ce nombre atteint 90, il retourne true. Sinon il retourne faux.
	 * @param button : boutons du plateau de jeu
	 * @param row : nombre de lignes
	 * @param col : nombre de colonnes
	 * @param numMines : nombre de mines choisi par le programme
	 * @return renvoie la valeur du bouleen
	 */
	public static boolean checkWin (JButton[][] button, int row, int col, int numMines)
	{
		boolean win = false;
		int count = 0;
		for (int x = 0; x < row; x++) {
			for (int y = 0; y < col; y++) {
				if (!button[x][y].isEnabled()) count++;
			}	
		}
		if (count == ((row*col)-numMines)) win = true ;
		return win;
	}

	/**
	 * methode lancer le jeu
	 */
	public static void begin ()

	{
		JOptionPane.showMessageDialog(null,"Entre les paramétres du jeu.", 
				"Bienvenue au démineur", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * methode lancer la fenetre de defaite
	 */
	public static void lose ()

	{
		JOptionPane.showMessageDialog(null,"Perdu. Essaie à nouveau.", 
				"Tu as perdu", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * methode lancer la fenetre de victoire
	 */
	public static void win ()

	{
		JOptionPane.showMessageDialog(null,"Tu as gagne.", 
				"Tu as gagne", JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Methode pour creer le plateau du Jeu
	 * @param plateau : plateau du jeu à creer
	 * @param row : nombre de lignes
	 * @param col : nombre de colonnes
	 * @param boutons : boutons du plateau de jeu
	 * @param appuiDroit : verification de l'appui droit
	 * @param mines : tableau de positionnement des mines
	 * @param numMines : nombre de mines choisi par le programme
	 * @param minesAlentours : tableau des chiffres indiquant les mines aux alentours
	 */
	public static void Setup (JPanel plateau, int row, int col, JButton[][] boutons, boolean[][] appuiDroit, 
			boolean[][] mines, int numMines, int[][] minesAlentours)
	//outil de creation du plateau de jeu
	//Organisation de la grille du jeu
	{plateau.setLayout(new GridLayout(row, col));
	for (int x = 0 ; x < row ; x++) {
		for (int y = 0; y < col ; y++) {
			//creation du bouton dans le plateau de jeu
			boutons[x][y]= new JButton();
			//creation d'une variable pour me dire si le clique droit est realise
			appuiDroit[x][y] = false;
			//ajoute les boutons au plateau de jeu
			plateau.add(boutons[x][y]);	
		}
	}
	Utilitaires.RemplirMines(mines, row, col, numMines);
	Utilitaires.RemplirNombres(minesAlentours, row, col, mines);
	}

	/** Methode definissant la saisie des valeurs utilisateurs dans les options
	 * @param fen = fenêtre du jeu
	 * @param sigh = icone sighed
	 * @param row = nombre de lignes;
	 * @param col = nombre de colonnes;
	 * @param numMines = nombre de mines du jeu
	 * @throws NumberFormatException = exception du bouton annuler;
	 */
		
}
