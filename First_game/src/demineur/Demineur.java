package demineur;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import utilitaires.Utilitaires;

@SuppressWarnings("serial")
/**
 * Creation du tableau du jeu du demineur, de son menu et des outils pour interagir avec
 * @author SRet
 *
 */
public class Demineur extends JFrame implements ActionListener, MouseListener{
	/*creation des variables du jeu
	 * - nombre de lignes
	 * - nombre de colonnes
	 * - nombre de mines
	 * - nombre de cases marques par l'utilisateur
	 * - existance des mines
	 * - bouton droit appuye ou non
	 * - creation des variables victoire et defaite
	 * - creation des differents menus
	 * - creation d'un texte qui dit "mines = nombre de mines choisis ; marque"
	 * - creations des boutons du jeu
	 * - creation du plateau où inserer les boutons
	 * - creation des varaibles icones du jeu
	 */

	int col = 10;
	int row = 10;
	int numMines = 10;	
	int marque = 0;
	boolean [][] mines = new boolean [row][col];
	boolean [][] appuiDroit = new boolean [row][col];
	boolean win = false;
	boolean lose = false;
	int [][] minesAlentours = new int [row][col];
	JMenuBar barreMenu = new JMenuBar();
	JMenu menu = new JMenu ("Menu Principal");
	JMenu help = new JMenu ("?");
	static JMenuItem newGame = new JMenuItem("Nouveau Jeu",'N');
	static JMenuItem options = new JMenuItem("options",'O');
	JMenuItem aide = new JMenuItem ("Aide", 'A');
	JMenuItem version = new JMenuItem ("Version", 'V');
	static JMenuItem exit = new JMenuItem("Quitter",'Q');
	JLabel mineLabel = new JLabel("mines: " +numMines+ " marked: "+marque);
	JButton [][] boutons = new JButton [row][col];
	JPanel plateau = new JPanel ();
	Icon bombe = new ImageIcon(getClass() .getResource("/mines.png"));
	Icon drapeau = new ImageIcon(getClass() .getResource("/flag.png"));
	Icon sigh = new ImageIcon(getClass() .getResource("/sigh.png"));
	Font PoliceCase = new Font ("Arial", Font.BOLD, 15);

	/**
	 * Constructeur de la JFrame Demineur
	 */
	public Demineur()

	//definition des parametres de la fenetre
	{setTitle("Demineur");
	setExtendedState(MAXIMIZED_BOTH);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setLocationRelativeTo(null);

	//Association alt+M au menu
	menu.setMnemonic('M');

	//code ajoutant les options au menu :
	//ajoute l'option Nouvelle Partie au menu principal
	newGame.addActionListener(this);
	menu.add(newGame);

	//ajoute l'option quitter au menu principal
	menu.add(options);
	options.addActionListener(this);

	//ajoute l'option quitter au menu principal
	menu.add(exit);
	exit.addActionListener(this);

	//ajoute l'opition aide au menu ?
	help.add(aide);
	aide.addActionListener(this);

	//ajoute l'option Version au menu ?
	help.add(version);
	version.addActionListener(this);

	//ajoute l'option menu principal à la barre de menu
	barreMenu.add(menu);
	barreMenu.add(help);

	/*ajoute le plateau de jeu (et le remplit) en faisant appel à une fonction utilitaire 
	 * et associe ce plateau aux outils pour lire les choix
	 */
	Utilitaires.Setup(plateau, row, col, boutons, appuiDroit, mines, numMines, minesAlentours);
	for (int x = 0; x < row; x++) {
		for (int y = 0; y < col; y++) {
			boutons[x][y].addActionListener(this);
			boutons[x][y].addMouseListener(this);	
		}
	}
	// ajoute le plateau cree au jeu
	add(plateau,BorderLayout.CENTER);

	//installe la barre de menu et la barre de status
	add(mineLabel, BorderLayout.SOUTH);
	setJMenuBar(barreMenu);
	}


	/**
	 * Methode definissant le comportement du jeu en nouvelle partie
	 */
	private void nouvellePartie() {
		dispose();
		this.remove(plateau);
		this.remove(mineLabel);
		plateau = new JPanel ();
		marque = 0;
		mines = new boolean [row][col];
		appuiDroit = new boolean [row][col];
		win = false;
		lose = false;
		minesAlentours = new int [row][col];
		mineLabel = new JLabel("mines: " +numMines+ " marked: "+marque);
		boutons = new JButton [row][col];
		Utilitaires.Setup(plateau, row, col, boutons, appuiDroit, mines, numMines, minesAlentours);
		for (int x = 0; x < row; x++) {
			for (int y = 0; y < col; y++) {
				boutons[x][y].addActionListener(this);
				boutons[x][y].addMouseListener(this);	
			}
		}
		add(plateau,BorderLayout.CENTER);
		add(mineLabel, BorderLayout.SOUTH);
		setJMenuBar(barreMenu);
		setVisible(true);
	}

	/**
	 * méthode qui definit la fenetre recommencer et ses fonctions
	 * oui relance une partie
	 * non quitte le jeu
	 */
	public static void beginAgain () {
		int reponse = JOptionPane.showConfirmDialog(null, "Voulez-vous recommencer?",
				"Recommencer", JOptionPane.YES_NO_OPTION);
		int reponse2 = -1;
		if (reponse==0)  reponse2 = JOptionPane.showConfirmDialog(null, "Voulez-vous changer la difficulté?",
				"Recommencer", JOptionPane.YES_NO_OPTION);
		if (reponse==1) exit.doClick();
		if (reponse2==0) options.doClick(); 
		if (reponse2==1) newGame.doClick();
	}

	/**
	 * action quand on utilise les options du menu et les boutons du jeu
	 */
	public void actionPerformed (ActionEvent e) {


		Object source = e.getSource();

		// lancer quitter
		if (source == exit) System.exit(0);

		//lancer options
		if (source == options)
			//demande à l'utilisateur de choisir le nombre de Lignes
		{int tempRow = row;
		int tempCol = col;
		int tempMines = numMines;
		//gestion de l'exception si l'utilisateur annule son choix ou entre une valeur lettree impossible a chiffrer
		try {
			int rowSaisi = 0;
			int colSaisi = 0;
			int minesSaisi = 0;

			//demande à l'utilisateur de choisir un nombre de colonnes entre 10 et 40
			do {
				rowSaisi = Integer.parseInt(JOptionPane.showInputDialog("Entrez un nombre de Lignes entre 10 et 40"));
				if (rowSaisi>40 || rowSaisi<10)
					JOptionPane.showMessageDialog(null, "Ce paramètre n'est pas atribué","Erreur",JOptionPane.ERROR_MESSAGE, sigh);}
			while (rowSaisi>40 || rowSaisi<10);

			//demande à l'utilisateur de choisir le nombre de colonnes entre 10 et 40
			do {
				colSaisi = Integer.parseInt(JOptionPane.showInputDialog("Entrez un nombre de Colonnes entre 10 et 40"));
				if (colSaisi>40 || colSaisi<10)
					JOptionPane.showMessageDialog(null, "Ce paramètre n'est pas atribué","Erreur",JOptionPane.ERROR_MESSAGE, sigh);}
			while (colSaisi>40 || colSaisi<10);

			//demande à l'utilisateur d'entrer un nombre de mines positif et inférieur au nombre de cases
			minesSaisi = Integer.parseInt(JOptionPane.showInputDialog("Entrez un nombre de mines inférieur à "+rowSaisi*colSaisi));

			//comportement du programme si l'utilisateur entre une mauvaise saisi
			do
			{if (minesSaisi == rowSaisi*colSaisi || minesSaisi == 0) 
			{JOptionPane.showMessageDialog(null, "Si tu entre ce nombre, il n'y a pas de jeu.","Attention",JOptionPane.ERROR_MESSAGE, sigh);
			minesSaisi = Integer.parseInt((String) JOptionPane.showInputDialog(null, "Nombres de mines inférieur à"+rowSaisi*colSaisi, "Mines",
					JOptionPane.PLAIN_MESSAGE, null, null,""));}
			else if (minesSaisi>rowSaisi*colSaisi)
			{JOptionPane.showMessageDialog(null, "Malheureusement, pas assez de cases pour ton nombre de mines voulu",
					"Attention",JOptionPane.ERROR_MESSAGE, sigh);
			minesSaisi = Integer.parseInt((String) JOptionPane.showInputDialog(null, "Nombres de mines inférieur à"+rowSaisi*colSaisi, "Mines",
					JOptionPane.PLAIN_MESSAGE, null, null,""));}}
			while (minesSaisi >= rowSaisi*colSaisi || minesSaisi == 0);

			//saisi les valeurs obtenus.
			row = rowSaisi;
			col = colSaisi;
			numMines = minesSaisi;
			nouvellePartie();}

		catch (NumberFormatException NFE)
		{JOptionPane.showInternalMessageDialog(null, "Tu as choisi le bouton annuler ou entrer une valeur non numérique."
				+ " Aucun changement n'est appliqué au jeu.");
		row = tempRow;
		col = tempCol;
		numMines = tempMines;}
		}

		//lancer Nouvelle partie
		if (source == newGame) nouvellePartie();

		//lancer aide
		if (source == aide) JOptionPane.showMessageDialog(null,"Le tableau est divisé en cases, avec des mines placées au hasard. "
				+ "Pour gagner, vous devez ouvrir toutes les cases qui ne contiennent pas de mines.", 
				"Aide", JOptionPane.INFORMATION_MESSAGE);

		//lancer Version
		if (source == version) JOptionPane.showMessageDialog(null,"Version béta du démineur."
				+ "\n réalisé par Retournay Steve (steve5488)", 
				"Version", JOptionPane.INFORMATION_MESSAGE) ;

		//action de cliquer sur une case
		for (int x = 0; x < row; x++) {
			for (int y = 0; y < col; y++) {
				if (source == boutons[x][y]) {
					actionClick(x, y, minesAlentours, row, col, mines, boutons, bombe);
					if (minesAlentours[x][y] == -1) lose = true;}
				else win = Utilitaires.checkWin(boutons, row, col, numMines);
				if (lose) {Utilitaires.lose();
				beginAgain();}
				//condition de victoire
				else if (win == true && lose == false) {Utilitaires.win();
				beginAgain();}	
			}
		}
	}

	/**
	 * methode qui definit ce qui passe quand on click sur un bouton
	 * @param x : ligne du bouton
	 * @param y : colonne du bouton
	 * @param minesAlentours : tableau du nombre de mines autour
	 * @param row : nombre de ligne
	 * @param col : nombre de colonnes
	 * @param mines : tableau des mines
	 * @param boutons : boutons sur lequel l'utilisateur clique pour jouer
	 * @param bombe : icone de la bombe
	 */

	private void actionClick (int x, int y, int[][] minesAlentours, int row, int col, boolean[][] mines, JButton[][] boutons, Icon bombe) {
		// bouton disparait 

		switch (minesAlentours[x][y]) {

		case -1 :
			for (int X = 0; X < row; X++) {
				for (int Y = 0; Y < col; Y++) {
					if (mines[X][Y] == true)
					{boutons[X][Y].setIcon(bombe);}
				}
			}
			break
			;

		case 0 :
			boutons[x][y].setEnabled(false);
			boutons[x][y].setText("");
			actionClickVide(x, y, boutons, minesAlentours, row, col);
			break;

		default :
			boutons[x][y].setEnabled(false);
			boutons[x][y].setFont(PoliceCase);
			boutons[x][y].setText(""+ minesAlentours[x][y]);
			break;	
		}
	}

	/**
	 * Methode qui definit ce qui se passe quand on clique sur une case vide
	 * @param x = position dans les lignes
	 * @param y = position dans les colonnes
	 * @param boutons = bouton du jeu
	 * @param row : nombre de lignes;
	 * @param col : nombre de colonnes;
	 * @param minesAlentours : tableau des mines aux alentours
	 */
	private void actionClickVide(int x, int y, JButton [][] boutons, int [][] minesAlentours, int row, int col) {

		// verifie la case au NO
		{if (x-1>=0 && y-1>=0 && boutons[x-1][y-1].isEnabled() == true)
		{if (minesAlentours[x-1][y-1] >= 0) actionClick(x-1, y-1, minesAlentours, row, col, mines, boutons, bombe);
		}

		// verifie la case à l'N
		if (x-1>=0 && boutons[x-1][y].isEnabled() == true)
		{if (minesAlentours[x-1][y] >= 0) actionClick(x-1, y, minesAlentours, row, col, mines, boutons, bombe);
		}

		// verifie la case au NE
		if (x-1>=0 && y+1<col && boutons[x-1][y+1].isEnabled() == true)
		{if (minesAlentours[x-1][y+1] >= 0) actionClick(x-1, y+1, minesAlentours, row, col, mines, boutons, bombe);
		}

		// verifie la case au O
		if (y-1>=0 && boutons[x][y-1].isEnabled() == true)
		{if (minesAlentours[x][y-1] >= 0) actionClick(x, y-1, minesAlentours, row, col, mines, boutons, bombe); }
		}

		// verifie la case a l'E
		if (y+1<col && boutons[x][y+1].isEnabled() == true)
		{if (minesAlentours[x][y+1] >= 0) actionClick(x, y+1, minesAlentours, row, col, mines, boutons, bombe);
		}

		// verifie la case au SO
		if (x+1<row && y-1>=0 && boutons[x+1][y-1].isEnabled() == true) 
		{if (minesAlentours[x+1][y-1] >= 0) actionClick(x+1, y-1, minesAlentours, row, col, mines, boutons, bombe); }

		//verifie la case à l'S
		if (x+1<row && boutons[x+1][y].isEnabled() == true)
		{if (minesAlentours[x+1][y] >= 0) actionClick(x+1, y, minesAlentours, row, col, mines, boutons, bombe); }

		//verifie la case au SE
		if (x+1<row && y+1<col && boutons[x+1][y+1].isEnabled() == true)
		{if (minesAlentours[x+1][y+1] >= 0) actionClick(x+1, y+1, minesAlentours, row, col, mines, boutons, bombe); }
	}

	/**
	 * methode qui explique le comportement du bouton droit
	 */
	public void mouseClicked(MouseEvent m) {
		// Action quand une case est clique avec le bouton droit
		if (m.getButton() == 3 ) {
			for (int x = 0; x < row; x++) {
				for (int y = 0; y < col; y++) {
					if (m.getSource() == boutons[x][y] && appuiDroit[x][y] == false && boutons[x][y].isEnabled() == true) {
						boutons[x][y].setIcon(drapeau);
						appuiDroit[x][y] = true;
						marque++;}
					else if (m.getSource() == boutons [x][y] && appuiDroit[x][y] == true) {
						boutons[x][y].setIcon(null);
						appuiDroit[x][y] = false;	
						marque--;}
					mineLabel.setText("mines: " +numMines+ " marked: "+marque);
				}
			}
		}
	}

	/**
	 * methode inutilisee dans le jeu
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * methode inutilisee dans le jeu
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * methode inutilisee dans le jeu
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * methode inutilisee dans le jeu
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}