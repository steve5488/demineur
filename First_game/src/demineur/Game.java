package demineur;

/**
 * Jeu du demineur
 * @author SRet
 *
 */
public class Game {
	
	/**
	 * methode de lancement du jeu
	 * @param args : lancement de l'application
	 */
	public static void main (String[] args)
	{Demineur fen = new Demineur();
	fen.setVisible(true);
	}

}
